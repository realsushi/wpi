+++
title = "Tomato grow"
description = ""
draft = true
date = 2019-10-10

[taxonomies]
tags = ["Float", "Zola"]

[extra]
feature_image = "Float.svg"
feature = true
link = ""
+++

## Why docker?
- provides a scripting feature
- provide an encapsulation method (no more virtual env...)
- standardization
- removes environment hazards (enables any new player instantly)
- enables orchestration with Kubernetes
- compatible with Ansible for cold boot install
- cross platform (almost seamlessly from OS and ARCH)

### Whole project and services
```shell
docker compose build
docker-compose up
```

### Frontedn Dockerfile
```shell
docker build -t 'frontend1' front/.
docker run -p 4200:4200 -d frontend1
```

### Backend Dockerfile
```shell
docker build -t 'backend1' back/
docker run -p 3306:3000 -d backend1
```

### Database Dockerfile
```
docker build -t 'database1' database/
```

### create docker container without Dockerfile
```
docker run --name some-mysql -v /my/own/datadir:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d mysql:tag
```

### what's next?
Once one is conformtable with docker build then it can be integrated into docker compose then refactored to kubernetes. ie, environment variables are refactored into kubernetes modules' parameters in YAML. K8s allows a complete automation, minus the initial push, which can be automated using Ansible and ssh protocol.