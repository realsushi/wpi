+++
title = "Tomato grow"
description = ""
draft = true
date = 2019-10-10

[taxonomies]
tags = ["Float", "Zola"]

[extra]
feature_image = "Float.svg"
feature = true
link = ""
+++


### Create and publish docker containers to Gitlab
- `docker login registry.gitlab.com`
- `docker build -t 'dockername' .`
- `docker tag dockername registry.gitlab.com/username/dockername:latest`
- `docker push registry.gitlab.com/username/dockername`