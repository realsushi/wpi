+++
title = "Tomato grow"
description = ""
draft = true
date = 2019-10-10

[taxonomies]
tags = ["Float", "Zola"]

[extra]
feature_image = "Float.svg"
feature = true
link = ""
+++

### Bash history cmds
```
gcloud init
gcloud auth list
gcloud info
gcloud config list
gcloud container clusters get-credentials clust-one

sudo apt-get install git
sudo apt-get install kubectl
kubectl
docker run hello-world
sudo apt install docker.io

kubectl config view
kubectl config current-context

kubectl create deployment hello-server --image=gcr.io/google-samples/hello-app:1.0
kubectl get deployment

kubectl apply -f result.configmap.yaml

kubectl create -f traefik-cr.yaml
kubectl create -f traefik-crb.yaml

echo -n 'supersecretword' | base64
```

### External doc
1. [Traefik](https://medium.com/kubernetes-tutorials/deploying-traefik-as-ingress-controller-for-your-kubernetes-cluster-b03a0672ae0c)

2. [Service type: ClusterIP/NodePort/LoadBalancer](https://medium.com/google-cloud/kubernetes-nodeport-vs-loadbalancer-vs-ingress-when-should-i-use-what-922f010849e0)

3. [create secret and call me maybe](https://opensource.com/article/19/6/introduction-kubernetes-secrets-and-configmaps)
`kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>`
