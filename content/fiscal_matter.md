+++
title = "Tomato grow"
description = ""
draft = true
date = 2019-10-10

[taxonomies]
tags = ["Float", "Zola"]

[extra]
feature_image = "Float.svg"
feature = true
link = ""
+++

### Crypto and fiscality

#### Definition
Trading and mining is associated with BNC (Bénéfices Non Commerciaux in France). It is not an intellectual service. 

#### Common case
private trading is taxed by Macron's flat tax at 30%.
Mining value is taxed on the declared day.

#### private society simple
Below 70k€ CA

#### private society complex
Above 70k CA