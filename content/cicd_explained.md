+++
title = "Tomato grow"
description = ""
draft = true
date = 2019-10-10

[taxonomies]
tags = ["Float", "Zola"]

[extra]
feature_image = "Float.svg"
feature = true
link = ""
+++

### CICD with gitlab

#### Main purpose
- Automate tasks
- verfify code quality (post commit)
- deploy stuff (this doc server)


#### How?
Setup a yaml file to define environments
each environment is a docker