+++
title = "Tomato grow"
description = ""
draft = true
date = 2019-10-10

[taxonomies]
tags = ["Float", "Zola"]

[extra]
feature_image = "Float.svg"
feature = true
link = ""
+++

### create secrets
#### simple keys
1. encode environment variables
`echo -n 'username' | base64`
2. sudo kubectl create secret APP_USER adsdvc"'('"(('ergfg

#### more complex key: docker pull secret
1. create access token

2. `export blabla = docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email> `

3. kubectl create secret $blabla

#### other way with files
1. encode file `kubectl create secret -f file.txt` 
