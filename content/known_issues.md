+++
title = "Tomato grow"
description = ""
draft = true
date = 2019-10-10

[taxonomies]
tags = ["Float", "Zola"]

[extra]
feature_image = "Float.svg"
feature = true
link = ""
+++

### App crash at npm start
1. EACCESS listen IP: PORT permission denied - 4032
- netsh interface ipv4 show excludedportrange protocol=tcp
- squash the hyper-V reserved ports by running script w10 excluded port range
- or reboot TWICE. check ports afterwards, they'll move above 50k

2. ECONNREFUSED - 4078
- start database (or docker desktop)
- docker start mariadbtestx
