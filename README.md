### The online documentation
#### INSTALL
1. clone
2. sudo npm i -g hexo-cli
REM: local install should work.

#### DEPLOY
1. git add .
2. git commit "ADD - DOC - new post"
3. git push

Requirements
- Assuming an ssh key/deploy token is provided into the git repository

#### Change theme
- edit config to an existing theme

#### Specifities
- Added scss renderer to make the theme work


#### TODO
Setup gitlab page URL
FIX broken links (out of the mount point)
Remove useless themes